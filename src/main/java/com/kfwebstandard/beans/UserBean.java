package com.kfwebstandard.beans;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.inject.Named;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

/**
 * This bean generates an event if the user has not logged in within the scope
 *
 * @author Ken
 */
@Named("user")
@SessionScoped
public class UserBean implements Serializable {

    private String name = "";
    private String password;
    private boolean loggedIn;

    public String getName() {
        return name;
    }

    public void setName(String newValue) {
        name = newValue;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String newValue) {
        password = newValue;
        if (password.equalsIgnoreCase("Moose")) {
            loggedIn = true;
        } else {
            loggedIn = false;
        }
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public String login() {
//        loggedIn = true;
        return "index";
    }

    public String logout() {
        loggedIn = false;
        return "login";
    }

    /**
     * Check for login and if not then go to login page
     *
     * @param event
     */
    public void checkLogin(ComponentSystemEvent event) {
        if (!loggedIn) {
            FacesContext context = FacesContext.getCurrentInstance();
            ConfigurableNavigationHandler handler = (ConfigurableNavigationHandler) context.getApplication().getNavigationHandler();
            handler.performNavigation("login");
        }
    }
}
